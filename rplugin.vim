" perl plugins


" node plugins


" python3 plugins
call remote#host#RegisterPlugin('python3', '/Users/magnus/.local/share/nvim/lazy/CopilotChat.nvim/rplugin/python3/CopilotChat', [
      \ {'sync': v:false, 'name': 'CopilotChatBuffer', 'type': 'command', 'opts': {'nargs': '1'}},
      \ {'sync': v:false, 'name': 'CopilotChat', 'type': 'command', 'opts': {'nargs': '1'}},
      \ {'sync': v:false, 'name': 'CopilotChatReset', 'type': 'command', 'opts': {}},
      \ {'sync': v:false, 'name': 'CopilotChatVisual', 'type': 'command', 'opts': {'nargs': '1', 'range': ''}},
      \ {'sync': v:false, 'name': 'CopilotChatVsplitToggle', 'type': 'command', 'opts': {}},
      \ {'sync': v:false, 'name': 'CopilotChatInPlace', 'type': 'command', 'opts': {'nargs': '*', 'range': ''}},
      \ {'sync': v:false, 'name': 'CopilotChatAutocmd', 'type': 'command', 'opts': {'nargs': '*'}},
      \ {'sync': v:false, 'name': 'CopilotChatMapping', 'type': 'command', 'opts': {'nargs': '*'}},
     \ ])


" ruby plugins


